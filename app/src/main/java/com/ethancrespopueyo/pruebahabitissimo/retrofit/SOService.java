package com.ethancrespopueyo.pruebahabitissimo.retrofit;

import com.ethancrespopueyo.pruebahabitissimo.models.CategoryModel;
import com.ethancrespopueyo.pruebahabitissimo.models.LocationModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SOService {

    @GET("category/list")
    Call<List<CategoryModel>> getCategories();

    @GET("location/list")
    Call<List<LocationModel>> getLocations();

    @GET("category/list/{category_id}")
    Call<List<CategoryModel>> getSubCategories(@Path("category_id") String category_id);

}