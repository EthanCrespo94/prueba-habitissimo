package com.ethancrespopueyo.pruebahabitissimo.retrofit;

public class ApiUtils {
    protected static final String BASE_URL = "http://api.habitissimo.es/";

    public static SOService getSOService() {
        return RetrofitClient.getClient().create(SOService.class);
    }
}