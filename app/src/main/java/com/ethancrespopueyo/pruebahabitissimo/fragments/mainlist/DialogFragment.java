package com.ethancrespopueyo.pruebahabitissimo.fragments.mainlist;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.ethancrespopueyo.pruebahabitissimo.MainActivity;
import com.ethancrespopueyo.pruebahabitissimo.R;
import com.ethancrespopueyo.pruebahabitissimo.common.CheckInternet;
import com.ethancrespopueyo.pruebahabitissimo.fragments.others.ErrorFragment;
import com.ethancrespopueyo.pruebahabitissimo.models.BudgetModel;
import com.ethancrespopueyo.pruebahabitissimo.models.CategoryModel;
import com.ethancrespopueyo.pruebahabitissimo.models.LocationModel;
import com.ethancrespopueyo.pruebahabitissimo.retrofit.ApiUtils;
import com.ethancrespopueyo.pruebahabitissimo.retrofit.SOService;
import com.ethancrespopueyo.pruebahabitissimo.sqlite.BDOpenHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DialogFragment extends android.support.v4.app.DialogFragment {

    private static final String TAG_ERROR = "ErrorFragment";

    private Button btn_add;
    EditText etName, etPhone, etMail, etDescr;

    private AutoCompleteTextView autoCompleteTextView;

    private AutocompleteAdapter adapter;
    private ArrayList<LocationModel> locationModelArrayList;
    private ArrayList<CategoryModel> categoryModelArrayList;
    private ArrayList<CategoryModel> subcategoryModelArrayList;
    private ArrayList<String> categoryNameArrayList;
    private ArrayList<String> subcategoryNameArrayList;

    View view;
    BDOpenHelper db;
    Context context;
    Spinner spinnerCategories;
    Spinner spinnerSubCategories;
    RelativeLayout rlSubcategories;


    public static DialogFragment newInstance() {
        DialogFragment fragment = new DialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(android.content.DialogInterface dialog,
                                     int keyCode, android.view.KeyEvent event) {
                    if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                        // To dismiss the fragment when the back-button is pressed.
                        dismiss();
                        return true;
                    }
                    // Otherwise, do nothing else
                    else return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        try {
            context = getContext();
            view = inflater.inflate(R.layout.fragment_dialog, container, false);
            view.findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeDialog();
                }
            });

            initializeDialog();

            initializeComponents();

            initializeCategorias();

            initializeLocations();

            btn_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addBudget();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void initializeComponents() {
        db = new BDOpenHelper(context);
        spinnerCategories = view.findViewById(R.id.spinnerCategories);
        spinnerSubCategories = view.findViewById(R.id.spinnerSubCategories);
        rlSubcategories = view.findViewById(R.id.rlSubcategories);
        etName = view.findViewById(R.id.etName);
        etPhone = view.findViewById(R.id.etPhone);
        etDescr = view.findViewById(R.id.etDescrip);
        etMail = view.findViewById(R.id.etEmail);
        btn_add = view.findViewById(R.id.btn_add);
        autoCompleteTextView = view.findViewById(R.id.autocompleteTxt);
        autoCompleteTextView.setThreshold(1);
        locationModelArrayList = new ArrayList<>();
        categoryNameArrayList = new ArrayList<>();
    }

    private void initializeDialog() {
        // Set transparent background and no title
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            // Para poder hacer scroll en el Dialog
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    private void initializeLocations() {
        //Locations autocomplete
        locationModelArrayList = db.getLocations();
        adapter = new AutocompleteAdapter(context, locationModelArrayList);
        autoCompleteTextView.setAdapter(adapter);
    }

    private void initializeCategorias() {
        //Spinner Categorias
        categoryModelArrayList = db.getCategories();

        for (int i = 0; i < categoryModelArrayList.size(); i++) {
            categoryNameArrayList.add(categoryModelArrayList.get(i).getName());
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (context, android.R.layout.simple_spinner_item,
                        categoryNameArrayList); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerCategories.setAdapter(spinnerArrayAdapter);
        spinnerCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getSubCategories(categoryModelArrayList.get(i).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getSubCategories(String categoryID) {
        subcategoryModelArrayList = new ArrayList<>();
        SOService service = ApiUtils.getSOService();
        Call<List<CategoryModel>> req = service.getSubCategories(categoryID);
        req.enqueue(new Callback<List<CategoryModel>>() {
            @Override
            public void onResponse(Call<List<CategoryModel>> call, Response<List<CategoryModel>> response) {
                try {
                    subcategoryModelArrayList = (ArrayList<CategoryModel>) response.body();
                    for (int i = 0; i < response.body().size(); i++)
                        Log.d("array98564:", subcategoryModelArrayList.get(i).getName());

                    subcategoryNameArrayList = new ArrayList<>();
                    for (int i = 0; i < subcategoryModelArrayList.size(); i++) {
                        subcategoryNameArrayList.add(subcategoryModelArrayList.get(i).getName());
                    }
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                            (context, android.R.layout.simple_spinner_item,
                                    subcategoryNameArrayList); //selected item will look like a spinner set from XML
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                            .simple_spinner_dropdown_item);
                    spinnerSubCategories.setAdapter(spinnerArrayAdapter);
                    rlSubcategories.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                    if (!CheckInternet.isNetworkConnected(context)) {
                        ((MainActivity) context).setFragment(new ErrorFragment(), TAG_ERROR);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CategoryModel>> call, Throwable t) {
                try {
                    if (!CheckInternet.isNetworkConnected(context))
                        ((MainActivity) context).setFragment(new ErrorFragment(), TAG_ERROR);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void closeDialog() {
        try {
            this.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return true;
        return false;
    }

    private void addBudget() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        if (isEmpty(etName)) {
            if (isEmpty(etMail) && android.util.Patterns.EMAIL_ADDRESS.matcher(etMail.getText().toString()).matches()) {
                if (isEmpty(etPhone)) {
                    if (isEmpty(etDescr)) {
                        BudgetModel budgetModel = new BudgetModel(etDescr.getText().toString(), subcategoryNameArrayList.get(spinnerSubCategories.getSelectedItemPosition()), etName.getText().toString(), etMail.getText().toString(), etPhone.getText().toString(), autoCompleteTextView.getText().toString());
                        MainFragment mainFragment = (MainFragment) getActivity().getSupportFragmentManager().findFragmentByTag("MainFragment");
                        mainFragment.addBadgeDialogCallback(budgetModel);
                        dismiss();
                    } else
                        Toast.makeText(context, "Por favor introduce una descripción.", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(context, "Por favor introduce un teléfono válido.", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(context, "Por favor introduce un correo válido.", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(context, "Por favor introduce un nombre", Toast.LENGTH_SHORT).show();
    }
}
