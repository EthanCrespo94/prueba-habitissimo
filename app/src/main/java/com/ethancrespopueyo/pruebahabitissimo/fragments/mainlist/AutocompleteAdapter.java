package com.ethancrespopueyo.pruebahabitissimo.fragments.mainlist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.ethancrespopueyo.pruebahabitissimo.R;
import com.ethancrespopueyo.pruebahabitissimo.models.LocationModel;

import java.util.ArrayList;
import java.util.List;

public class AutocompleteAdapter extends ArrayAdapter<LocationModel> {
    @SuppressWarnings("unchecked")

    ArrayList<LocationModel> locationModelArrayList;

    AutocompleteAdapter(Context context, ArrayList<LocationModel> filteredList) {
        super(context, 0, filteredList);
        locationModelArrayList = filteredList;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }

    @SuppressWarnings("unchecked")
    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            List<LocationModel> suggestions = new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0) {
                suggestions.addAll(locationModelArrayList);
            } else {
                String filterPattern = charSequence.toString().toLowerCase().trim();
                for (LocationModel item : locationModelArrayList) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        suggestions.add(item);
                    } else if (item.getZip().toLowerCase().contains(filterPattern))
                        suggestions.add(item);
                }
            }
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            if (filterResults.count == 0) {
                notifyDataSetInvalidated();
            } else {
                clear();
                addAll((List) filterResults.values);
                notifyDataSetChanged();
            }
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((LocationModel) resultValue).getName();
        }
    };

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //initialize view
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.autocomplete_item, parent, false);
        }
        //get current object from SomeList class
        LocationModel currentSomeList = getItem(position);


        TextView mFirstTextView = (TextView) listItemView.findViewById(R.id.text_view_name);
        TextView mSecondTextView = (TextView) listItemView.findViewById(R.id.text_view_zip);

        if (currentSomeList != null) {
            mFirstTextView.setText(String.valueOf(currentSomeList.getName()));
            mSecondTextView.setText(currentSomeList.getZip());
        }

        return listItemView;
    }
}