package com.ethancrespopueyo.pruebahabitissimo.fragments.mainlist;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ethancrespopueyo.pruebahabitissimo.R;
import com.ethancrespopueyo.pruebahabitissimo.models.BudgetModel;
import com.ethancrespopueyo.pruebahabitissimo.sqlite.BDOpenHelper;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    ListAdapter adapter;
    RecyclerView recyclerView;
    ArrayList<BudgetModel> budgetModelArrayList;
    ArrayList<BudgetModel> budgetModelArrayListFull;

    BDOpenHelper bd;
    Context context;
    Activity activity;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_list, container, false);

        initializeComponents();

        initializeRecyclerView();

        return view;
    }

    private void initializeRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new ListAdapter(recyclerView, activity, budgetModelArrayList);
        recyclerView.setAdapter(adapter);

        adapter.setLoadMore(new ILoadMore() {
            @Override
            public void onLoadMore() {

                if (adapter != null)
                    if (budgetModelArrayList.size() <= budgetModelArrayListFull.size()) {
                        budgetModelArrayList.add(null);
                        recyclerView.post(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyItemInserted(budgetModelArrayList.size() - 1);

                            }
                        });

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                budgetModelArrayList.remove(budgetModelArrayList.size() - 1);
                                adapter.notifyItemRemoved(budgetModelArrayList.size());

                                int index = budgetModelArrayList.size();
                                int end = index + 10; //Los carga de 10 en 10

                                if (budgetModelArrayListFull.size() < end)
                                    end = budgetModelArrayListFull.size();

                                for (int i = index; i < end; i++) {
                                    budgetModelArrayList.add(budgetModelArrayListFull.get(i));
                                }

                                adapter.notifyDataSetChanged();

                                if (end != budgetModelArrayListFull.size())
                                    adapter.setLoading();

                            }
                        }, 1000);
                    }


            }
        });

        if (budgetModelArrayListFull.size() > 0) {
            budgetModelArrayList.add(budgetModelArrayListFull.get(0));
            adapter.notifyDataSetChanged();
        }
    }

    private void initializeComponents() {
        context = getContext();
        activity = getActivity();
        bd = new BDOpenHelper(context);
        view.findViewById(R.id.add_button).setOnClickListener(this);

        budgetModelArrayList = new ArrayList<>();
        budgetModelArrayListFull = bd.getBudgets();
        recyclerView = view.findViewById(R.id.recycler_view);
    }

    public void addBadgeDialogCallback(BudgetModel budgetModel) {
        budgetModelArrayList.add(budgetModel);
        adapter.notifyDataSetChanged();
        bd.insertBudget(budgetModel);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        DialogFragment dialog = DialogFragment.newInstance();
        dialog.show(getActivity().getSupportFragmentManager(), "DIALOG");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
