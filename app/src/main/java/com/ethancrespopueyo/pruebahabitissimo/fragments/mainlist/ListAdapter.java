package com.ethancrespopueyo.pruebahabitissimo.fragments.mainlist;
import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ethancrespopueyo.pruebahabitissimo.R;
import com.ethancrespopueyo.pruebahabitissimo.models.BudgetModel;
import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;

        ILoadMore loadMore;
        boolean isLoading;
        Activity activity;
        List<BudgetModel> list_items;
        int visibleTreshold = 5;
        int lastVisibleItem;
        int totalItemCount;

        public ListAdapter(RecyclerView recyclerView, Activity activity, ArrayList<BudgetModel> list_items) {

            this.activity = activity;
            this.list_items = list_items;

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleTreshold)) {

                        if (loadMore != null) {
                            loadMore.onLoadMore();
                            isLoading = true;

                        }
                    }

                }
            });
        }

        @Override
        public int getItemViewType(int position) {
            return list_items.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        public void setLoadMore(ILoadMore loadMore) {
            this.loadMore = loadMore;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(activity).inflate(R.layout.item_layout, parent, false);
                return new ItemViewHolder(view);
            }else if(viewType == VIEW_TYPE_LOADING){
                View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
                return new LoadingViewHolder(view);
            }

            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            if(holder instanceof ItemViewHolder){
                SpannableStringBuilder str = new SpannableStringBuilder(list_items.get(position).getName());
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((ItemViewHolder) holder).name.setText(str + " - " + list_items.get(position).getSubcategory());
                str = new SpannableStringBuilder("Teléfono: ");
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                SpannableStringBuilder cursiva = new SpannableStringBuilder(list_items.get(position).getPhone());
                cursiva.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.ITALIC), 0, cursiva.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((ItemViewHolder) holder).phone.setText(str +""+ cursiva + " (" + list_items.get(position).getLocation() + ")");
                str = new SpannableStringBuilder("E-mail: ");
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((ItemViewHolder) holder).mail.setText(str + list_items.get(position).getEmail());
                str = new SpannableStringBuilder("Descripción:\n");
                str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((ItemViewHolder) holder).descr.setText(str + list_items.get(position).getDescription());

            }else if(holder instanceof LoadingViewHolder){

                ((LoadingViewHolder) holder).progressBar.setIndeterminate(true);
            }

        }

        public void setLoading() {
            isLoading = false;
        }

        @Override
        public int getItemCount() {
            return list_items.size();
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder {

        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);

            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView name, phone, mail, descr;

        public ItemViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.text_name);
            phone = itemView.findViewById(R.id.text_phone);
            mail = itemView.findViewById(R.id.text_mail);
            descr = itemView.findViewById(R.id.text_desc);
        }
    }

