package com.ethancrespopueyo.pruebahabitissimo.fragments.others;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.ethancrespopueyo.pruebahabitissimo.MainActivity;
import com.ethancrespopueyo.pruebahabitissimo.R;
import com.ethancrespopueyo.pruebahabitissimo.common.GlideApp;
import com.ethancrespopueyo.pruebahabitissimo.fragments.mainlist.MainFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoadFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoadFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoadFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG_MAIN = "MainFragment";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LoadFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoadFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoadFragment newInstance(String param1, String param2) {
        LoadFragment fragment = new LoadFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            // Inflate the layout for this fragment
            view = inflater.inflate(R.layout.fragment_load, container, false);
            return view;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    Context context;
    ImageView ivL;
    TextView tvC;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeComponents();

        new AnimationsAsyncTask().execute();

    }

    private void initializeComponents(){
        context = getContext();
        tvC = view.findViewById(R.id.textViewTitle);
        ivL = view.findViewById(R.id.habitissimo_web_logo);
    }

    private class AnimationsAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            Animation slideR = AnimationUtils.loadAnimation(context, R.anim.slide_in_right);
            slideR.reset();
            tvC.startAnimation(slideR);
            try {
                Thread.sleep(1400);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadHabitisimoLogo();

                        Animation transparent = AnimationUtils.loadAnimation(context, R.anim.transparent);
                        transparent.reset();
                        ivL.startAnimation(transparent);
                    }

                });
                Thread.sleep(2500);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) context).setFragment(new MainFragment(), TAG_MAIN);
                    }
                });

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    public void loadHabitisimoLogo() {
        try {
            GlideApp.with(this).load(R.drawable.web_logo)
                    .fitCenter()
                    .into(ivL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
