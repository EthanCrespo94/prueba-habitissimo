package com.ethancrespopueyo.pruebahabitissimo.fragments.mainlist;

public interface ILoadMore {
    void onLoadMore();
}
