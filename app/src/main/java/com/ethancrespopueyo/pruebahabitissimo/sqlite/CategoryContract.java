package com.ethancrespopueyo.pruebahabitissimo.sqlite;

import android.provider.BaseColumns;

final class CategoryContract {

    CategoryContract(){}

    public class categoryContract implements BaseColumns {
        public static final String TABLE_NAME  = "CATEGORY";
        public static final String COL_ID  = "ID";
        public static final String COL_PARENT_ID = "PARENT_ID";
        public static final String COL_NAME = "NAME";
        public static final String COL_NORMALIZED_NAME = "NORMALIZED_NAME";
        public static final String COL_DESCRIPTION = "DESCRIPTION";
        public static final String COL_CHILD_COUNT = "CHILD_COUNT";
        public static final String COL_CHILDREN = "CHILD_CHILDREN";
        public static final String COL_ICON = "CHILD_ICON";
    }
}
