package com.ethancrespopueyo.pruebahabitissimo.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ethancrespopueyo.pruebahabitissimo.models.BudgetModel;
import com.ethancrespopueyo.pruebahabitissimo.models.CategoryModel;
import com.ethancrespopueyo.pruebahabitissimo.models.LocationModel;

import java.util.ArrayList;

public class BDOpenHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "PRUEBAHABITISSIMO.db";

    private static final String SQL_CREATE_CATEGORY = "CREATE TABLE IF NOT EXISTS " + CategoryContract.categoryContract.TABLE_NAME + "(" +
            CategoryContract.categoryContract.COL_ID + " TEXT PRIMARY KEY NOT NULL," +
            CategoryContract.categoryContract.COL_PARENT_ID + " TEXT," +
            CategoryContract.categoryContract.COL_NAME + " TEXT ," +
            CategoryContract.categoryContract.COL_NORMALIZED_NAME + " TEXT, " +
            CategoryContract.categoryContract.COL_DESCRIPTION + " TEXT, " +
            CategoryContract.categoryContract.COL_CHILD_COUNT + " INTEGER, " +
            CategoryContract.categoryContract.COL_CHILDREN + " TEXT, " +
            CategoryContract.categoryContract.COL_ICON + " TEXT " +
            ");";

    private static final String SQL_CREATE_BUDGET = "CREATE TABLE IF NOT EXISTS " + BudgetContract.budgetContract.TABLE_NAME + "(" +
            BudgetContract.budgetContract.COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ," +
            BudgetContract.budgetContract.COL_NAME + " TEXT," +
            BudgetContract.budgetContract.COL_SUBCATEGORY + " TEXT ," +
            BudgetContract.budgetContract.COL_LOCATION_NAME + " TEXT, " +
            BudgetContract.budgetContract.COL_DESCRIPTION + " TEXT, " +
            BudgetContract.budgetContract.COL_EMAIL + " INTEGER, " +
            BudgetContract.budgetContract.COL_PHONE + " TEXT " +
            ");";

    private static final String SQL_CREATE_LOCATION = "CREATE TABLE IF NOT EXISTS " + LocationContract.locationContract.TABLE_NAME + "(" +
            LocationContract.locationContract.COL_ID + " TEXT PRIMARY KEY NOT NULL," +
            LocationContract.locationContract.COL_PARENT_ID + " TEXT," +
            LocationContract.locationContract.COL_NAME + " TEXT, " +
            LocationContract.locationContract.COL_ZIP + " INTEGER, " +
            LocationContract.locationContract.COL_GEO_LAT + " DOUBLE ," +
            LocationContract.locationContract.COL_LEVEL + " INTEGER, " +
            LocationContract.locationContract.COL_GEO_LNG + " DOUBLE ," +
            LocationContract.locationContract.COL_CHILDREN + " TEXT ," +
            LocationContract.locationContract.COL_SLUG + " TEXT" +
            ");";

    public BDOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            db.execSQL(SQL_CREATE_CATEGORY);
            db.execSQL(SQL_CREATE_LOCATION);
            db.execSQL(SQL_CREATE_BUDGET);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        try {
            if (newVersion > oldVersion) {
                db.execSQL("DROP TABLE IF EXISTS " + CategoryContract.categoryContract.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + LocationContract.locationContract.TABLE_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + BudgetContract.budgetContract.TABLE_NAME);
                onCreate(db);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * ------------------------- Inserts-------------------------
     */
    public void insertCategories(ArrayList<CategoryModel> categoryModelArrayList) {
        if (checkCategories())
            deleteCategories();
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();

        for (CategoryModel cm : categoryModelArrayList) {
            cv.put(CategoryContract.categoryContract.COL_ID, cm.getId());
            cv.put(CategoryContract.categoryContract.COL_PARENT_ID, cm.getParentId());
            cv.put(CategoryContract.categoryContract.COL_NAME, cm.getName());
            cv.put(CategoryContract.categoryContract.COL_NORMALIZED_NAME, cm.getNormalizedName());
            cv.put(CategoryContract.categoryContract.COL_DESCRIPTION, cm.getDescription());
            cv.put(CategoryContract.categoryContract.COL_CHILD_COUNT, cm.getChildCount());
            cv.put(CategoryContract.categoryContract.COL_CHILDREN, cm.getChildren());
            cv.put(CategoryContract.categoryContract.COL_ICON, cm.getIcon());
            db.insert(CategoryContract.categoryContract.TABLE_NAME, null, cv);
        }
        db.close();
    }

    public void insertLocations(ArrayList<LocationModel> locationModelArrayList) {
        if (checkLocations())
            deleteLocations();
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();

        for (LocationModel lm : locationModelArrayList) {
            cv.put(LocationContract.locationContract.COL_ID, lm.getId());
            cv.put(LocationContract.locationContract.COL_PARENT_ID, lm.getParentId());
            cv.put(LocationContract.locationContract.COL_NAME, lm.getName());
            cv.put(LocationContract.locationContract.COL_ZIP, lm.getZip());
            cv.put(LocationContract.locationContract.COL_GEO_LAT, lm.getGeoLat());
            cv.put(LocationContract.locationContract.COL_LEVEL, lm.getLevel());
            cv.put(LocationContract.locationContract.COL_GEO_LNG, lm.getGeoLng());
            cv.put(LocationContract.locationContract.COL_CHILDREN, lm.getChildren());
            cv.put(LocationContract.locationContract.COL_SLUG, lm.getSlug());
            db.insert(LocationContract.locationContract.TABLE_NAME, null, cv);

        }
        db.close();
    }

    public void insertBudget(BudgetModel budgetModel) {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = this.getWritableDatabase();

        cv.put(BudgetContract.budgetContract.COL_SUBCATEGORY, budgetModel.getSubcategory());
        cv.put(BudgetContract.budgetContract.COL_NAME, budgetModel.getName());
        cv.put(BudgetContract.budgetContract.COL_PHONE, budgetModel.getPhone());
        cv.put(BudgetContract.budgetContract.COL_EMAIL, budgetModel.getEmail());
        cv.put(BudgetContract.budgetContract.COL_DESCRIPTION, budgetModel.getDescription());
        cv.put(BudgetContract.budgetContract.COL_LOCATION_NAME, budgetModel.getLocation());

        db.insert(BudgetContract.budgetContract.TABLE_NAME, null, cv);
        db.close();
    }

    /**
     * ------------------------- Selects-------------------------
     */

    public boolean checkCategories() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CategoryContract.categoryContract.TABLE_NAME);
        db.close();
        return numRows != 0;
    }

    public boolean checkLocations() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, LocationContract.locationContract.TABLE_NAME);
        db.close();
        return numRows != 0;
    }

    public ArrayList<LocationModel> getLocations() {
        ArrayList<LocationModel> locationModelArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT " + LocationContract.locationContract.COL_NAME + "," + LocationContract.locationContract.COL_ZIP + " FROM " + LocationContract.locationContract.TABLE_NAME, null);
        if (res.moveToFirst()) {
            do {
                LocationModel locationModel = new LocationModel();
                locationModel.setName(res.getString(0));
                locationModel.setZip(res.getString(1));
                locationModelArrayList.add(locationModel);
                Log.d("arrayLocationdb:", locationModel.getName());
            }
            while (res.moveToNext());
        }
        res.close();
        db.close();
        return locationModelArrayList;
    }

    public ArrayList<CategoryModel> getCategories() {
        ArrayList<CategoryModel> locationModelArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT " + CategoryContract.categoryContract.COL_NAME + "," + CategoryContract.categoryContract.COL_ID + " FROM " + CategoryContract.categoryContract.TABLE_NAME, null);
        if (res.moveToFirst()) {
            do {
                CategoryModel categoryModel = new CategoryModel();
                categoryModel.setName(res.getString(0));
                categoryModel.setId(res.getString(1));
                locationModelArrayList.add(categoryModel);
                Log.d("CategoryModeldb:", categoryModel.getName());
            }
            while (res.moveToNext());
        }
        res.close();
        db.close();
        return locationModelArrayList;
    }


    public ArrayList<BudgetModel> getBudgets() {
        ArrayList<BudgetModel> budgetModelArrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + BudgetContract.budgetContract.TABLE_NAME, null);
        if (res.moveToFirst()) {
            do {
                BudgetModel budgetModel = new BudgetModel(res.getString(4), res.getString(2), res.getString(1), res.getString(5), res.getString(6), res.getString(3));
                budgetModelArrayList.add(budgetModel);
                Log.d("CategoryModeldb:", budgetModel.toString());
            }
            while (res.moveToNext());
        }
        res.close();
        db.close();
        return budgetModelArrayList;
    }


    /**
     * ------------------------- Deletes-------------------------
     */
    public void deleteCategories() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(CategoryContract.categoryContract.TABLE_NAME, null, null);
        db.close();
    }

    public void deleteLocations() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(LocationContract.locationContract.TABLE_NAME, null, null);
        db.close();
    }
}
