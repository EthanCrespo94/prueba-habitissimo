package com.ethancrespopueyo.pruebahabitissimo.sqlite;

import android.provider.BaseColumns;

final class LocationContract {

    LocationContract(){}

    public class locationContract implements BaseColumns {
        public static final String TABLE_NAME  = "LOCATION";
        public static final String COL_ID  = "ID";
        public static final String COL_PARENT_ID = "PARENT_ID";
        public static final String COL_NAME = "NAME";
        public static final String COL_ZIP = "ZIP";
        public static final String COL_GEO_LAT = "GEO_LAT";
        public static final String COL_LEVEL = "LEVEL";
        public static final String COL_GEO_LNG = "GEO_LNG";
        public static final String COL_CHILDREN = "CHILDREN";
        public static final String COL_SLUG = "SLUG";
    }
}
