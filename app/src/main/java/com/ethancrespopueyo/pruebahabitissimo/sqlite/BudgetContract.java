package com.ethancrespopueyo.pruebahabitissimo.sqlite;

import android.provider.BaseColumns;

final class BudgetContract {

    BudgetContract(){}

    public class budgetContract implements BaseColumns {
        public static final String TABLE_NAME  = "BUDGETS";
        public static final String COL_ID  = "ID";
        public static final String COL_SUBCATEGORY = "SUBCATEGORY";
        public static final String COL_NAME = "NAME";
        public static final String COL_PHONE = "PHONE";
        public static final String COL_LOCATION_NAME = "LOCATION_NAME";
        public static final String COL_EMAIL = "EMAIL";
        public static final String COL_DESCRIPTION = "CHILD_CHILDREN";
    }
}
