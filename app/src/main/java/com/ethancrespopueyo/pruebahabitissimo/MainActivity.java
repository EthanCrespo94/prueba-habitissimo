package com.ethancrespopueyo.pruebahabitissimo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import com.ethancrespopueyo.pruebahabitissimo.common.CheckInternet;
import com.ethancrespopueyo.pruebahabitissimo.fragments.mainlist.MainFragment;
import com.ethancrespopueyo.pruebahabitissimo.fragments.others.ErrorFragment;
import com.ethancrespopueyo.pruebahabitissimo.fragments.others.LoadFragment;
import com.ethancrespopueyo.pruebahabitissimo.models.CategoryModel;
import com.ethancrespopueyo.pruebahabitissimo.models.LocationModel;
import com.ethancrespopueyo.pruebahabitissimo.retrofit.ApiUtils;
import com.ethancrespopueyo.pruebahabitissimo.retrofit.SOService;
import com.ethancrespopueyo.pruebahabitissimo.sqlite.BDOpenHelper;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements LoadFragment.OnFragmentInteractionListener , MainFragment.OnFragmentInteractionListener , ErrorFragment.OnFragmentInteractionListener {

    public FragmentManager fragmentManager;
    public ArrayList<CategoryModel> categoryModelArrayList;
    public ArrayList<LocationModel> locationModelArrayList;

    private static final String TAG_ERROR = "ErrorFragment";

    static BDOpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new BDOpenHelper(this);
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        LoadFragment loadFragment = new LoadFragment();
        fragmentTransaction.add(R.id.fragmentContainer, loadFragment, null);
        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new DownloadAsyncTask().execute();
    }

    private class DownloadAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            if (CheckInternet.isNetworkConnected(getApplicationContext())) {
                getCategories();
            } else {
                setFragment(new ErrorFragment(), TAG_ERROR);
            }
            return null;
        }

    }
    private void getCategories() {
        categoryModelArrayList = new ArrayList<>();
        SOService service = ApiUtils.getSOService();
        Call<List<CategoryModel>> req = service.getCategories();
        req.enqueue(new Callback<List<CategoryModel>>() {
            @Override
            public void onResponse(Call<List<CategoryModel>> call, Response<List<CategoryModel>> response) {
                try {
                    categoryModelArrayList = (ArrayList<CategoryModel>) response.body();
                    for (int i = 0; i < response.body().size(); i++)
                        Log.d("array:", categoryModelArrayList.get(i).getName());
                    db.insertCategories(categoryModelArrayList);
                    getLocations();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!CheckInternet.isNetworkConnected(getApplicationContext())) {
                        setFragment(new ErrorFragment(), TAG_ERROR);
                    }
                }

            }

            @Override
            public void onFailure(Call<List<CategoryModel>> call, Throwable t) {

                try {
                    if (!CheckInternet.isNetworkConnected(getApplicationContext())) {
                        setFragment(new ErrorFragment(), TAG_ERROR);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getLocations() {
        locationModelArrayList = new ArrayList<>();
        SOService service = ApiUtils.getSOService();
        Call<List<LocationModel>> req = service.getLocations();
        req.enqueue(new Callback<List<LocationModel>>() {
            @Override
            public void onResponse(Call<List<LocationModel>> call, Response<List<LocationModel>> response) {
                try {
                    locationModelArrayList = (ArrayList<LocationModel>) response.body();
                    for (int i = 0; i < locationModelArrayList.size(); i++)
                        Log.d("arrayLocation:", locationModelArrayList.get(i).getName());
                    db.insertLocations(locationModelArrayList);
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!CheckInternet.isNetworkConnected(getApplicationContext())) {
                        setFragment(new ErrorFragment(), TAG_ERROR);
                    }
                }

            }

            @Override
            public void onFailure(Call<List<LocationModel>> call, Throwable t) {

                try {
                    if (!CheckInternet.isNetworkConnected(getApplicationContext())) {
                        setFragment(new ErrorFragment(), TAG_ERROR);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setFragment(Fragment fragment, String tag) {
        try {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentContainer, fragment, tag);
            transaction.addToBackStack(null);  // this will manage backstack
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
            if (f instanceof LoadFragment)
                System.exit(0);
            else if(f instanceof MainFragment)
                exitAlertDialog();
            else {
                if (getFragmentManager().getBackStackEntryCount() > 0) {
                    getFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exitAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Salir de " + getString(R.string.app_name));
        builder.setMessage("¿Seguro que desea salir de "+ getString(R.string.app_name)+"?");

        // Add the buttons
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                moveTaskToBack(true);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        // Create the AlertDialog
        AlertDialog dialog = builder.create();

        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(this.getResources().getColor(R.color.colorPrimary));
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(this.getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }
}
