package com.ethancrespopueyo.pruebahabitissimo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationModel {

    public LocationModel(){}

    public LocationModel(Integer id, Integer parentId, String name, String zip, Double geoLat, Integer level, Double geoLng, String children, String slug) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.zip = zip;
        this.geoLat = geoLat;
        this.level = level;
        this.geoLng = geoLng;
        this.children = children;
        this.slug = slug;
    }

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("parent_id")
    @Expose
    private Integer parentId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("geo_lat")
    @Expose
    private Double geoLat;
    @SerializedName("level")
    @Expose
    private Integer level;
    @SerializedName("geo_lng")
    @Expose
    private Double geoLng;
    @SerializedName("children")
    @Expose
    private String children;
    @SerializedName("slug")
    @Expose
    private String slug;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public Double getGeoLat() {
        return geoLat;
    }

    public void setGeoLat(Double geoLat) {
        this.geoLat = geoLat;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Double getGeoLng() {
        return geoLng;
    }

    public void setGeoLng(Double geoLng) {
        this.geoLng = geoLng;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

}
