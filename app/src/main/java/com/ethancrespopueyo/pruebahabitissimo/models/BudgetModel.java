package com.ethancrespopueyo.pruebahabitissimo.models;

public class BudgetModel {

    String description, subcategory, name, email, phone, location;

    int id;

    public BudgetModel() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public BudgetModel(String description, String subcategory, String name, String email, String phone, String location, int id) {
        this.description = description;
        this.subcategory = subcategory;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.location = location;
        this.id = id;

    }
    public BudgetModel(String description, String subcategory, String name, String email, String phone, String location) {
        this.description = description;
        this.subcategory = subcategory;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.location = location;
        this.id = id;

    }

    @Override
    public String toString() {
        return "BudgetModel{" +
                "description='" + description + '\'' +
                ", subcategory='" + subcategory + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", location='" + location + '\'' +
                ", id=" + id +
                '}';
    }
}
